
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;

import org.json.simple.JSONObject;


public class j {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			boolean terminar = false;
			// Crea el socket del cliente
			Socket clientSocket = new Socket();
			System.out.println("Estableciendo la conexi�n");
			// Crea la direcci�n del servidor con el que se comunica
			InetSocketAddress addr = new InetSocketAddress("localhost", 5000);
			// Se conecta al servidor con esa IP y puerto
			clientSocket.connect(addr);
			// Creamos los Stream para enviar y recoger los mensajes con el
			// Input y Output del socket
			InputStream is = clientSocket.getInputStream();
			OutputStream os = clientSocket.getOutputStream();
			BufferedReader brRespuesta = new BufferedReader(new InputStreamReader(is));
			PrintWriter out = new PrintWriter(os);
			BufferedReader brEntrada = new BufferedReader(new InputStreamReader(System.in));
			
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("cmd", "identificacion");
			System.out.println("Introducir el nombre de usuario:  ");
			String userName=brEntrada.readLine();
			jsonObj.put("userName", userName);
			
			out.println(jsonObj.toString());
			//vacia el buffer
			out.flush();
			while (!terminar) {
				
				jsonObj.put("cmd", "mensaje");
				jsonObj.put("emisor", userName);
				System.out.println("nombre de receptor:");
				jsonObj.put("receptor", brEntrada.readLine());
				System.out.println("escribir la mensaje:");
				jsonObj.put("contenido", brEntrada.readLine());
				out.println(jsonObj.toString());
				//vacia el buffer
				out.flush();
				
				
				System.out.println(userName+"----Repuesta : " + brRespuesta.readLine());
				
				
				
			}
			//cerra el socket de cliente
			System.out.println("Cerrando el socket cliente");
			clientSocket.close();
			System.out.println("Terminado");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
