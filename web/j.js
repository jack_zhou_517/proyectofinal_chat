net = require('net');

var clients = [];

net.createServer(function (socket) {

  socket.Ip = socket.remoteAddress + ":" + socket.remotePort 
 
  clients.push(socket);
 console.log(socket.Ip + " conecta a chat");


  socket.on('data', function (data) {
  	var dato_enviado=JSON.parse(data.toString('utf8').trim());
  	// process.stdout.write(data);
  	//console.log(dato_enviado.cmd + " :comando.\n");
  	if(dato_enviado.cmd=='identificacion'){
  		socket.name=dato_enviado.userName;
  		console.log(dato_enviado.userName+" :entra a servidor");
  	}else if(dato_enviado.cmd=='mensaje'){
  		console.log(dato_enviado.emisor+" ---enviador para "+dato_enviado.receptor+" : "+dato_enviado.contenido+"\n");
  		comunicacion(dato_enviado.emisor,dato_enviado.receptor,dato_enviado.contenido);
  	}else{
  		console.log("no existe este comando");
  	}
   
  });
  socket.on('error', function () {
    
    console.log(socket.name + " error\n");

  });

  socket.on('end', function () {
    clients.splice(clients.indexOf(socket), 1);
    console.log(socket.name + " left the chat.\n");

  });
  function comunicacion(emisor,receptor,contenido){
 clients.forEach(function (client) {
      // Don't want to send it to sender
      if (client.name === receptor) 
      client.write(emisor+","+contenido+"\n");
    });
  }
  
  
}).listen(5000);

// Put a friendly message on the terminal of the server.
console.log("Chat server running at port 5000\n");