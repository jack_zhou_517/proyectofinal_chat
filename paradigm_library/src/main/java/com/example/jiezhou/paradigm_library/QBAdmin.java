package com.example.jiezhou.lib.comunicacion_quickblox;

import android.os.Bundle;
import android.util.Log;

import com.quickblox.auth.QBAuth;
import com.quickblox.auth.model.QBSession;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.QBSettings;
import com.quickblox.users.model.QBUser;

import java.util.List;

/**
 * Created by jie.zhou on 06/11/2015.
 * la clase que gestiona los metodos necesario para efectuar la comunicacion con QuickBlox
 */
public class QBAdmin {
    QBAdminListeners listener;

    boolean datos_conexion_principal = false;

    /**
     * este clase sirve para conecta la aplicacion que esta en quickblox
     */
    public void conexion_aplicacion_quickBlox(String id, String aut_key, String aut_secr) {
        QBSettings.getInstance().fastConfigInit(id, aut_key, aut_secr);
        Log.v("test QBAdmin", "entrado a la apliccacion de");

    }

    /**
     *escuchar los eventos
     * @param Listener
     */
    public void addListeners(QBAdminListeners Listener) {
        this.listener = Listener;
    }


    /**
     * comprobacion de la sesion principal conectado correctamente o no
     */
    public void session_principal() {
        QBAuth.createSession(new QBEntityCallbackImpl<QBSession>() {
            @Override
            public void onSuccess(QBSession session, Bundle params) {
                // success
                datos_conexion_principal = true;
                listener.Session_principal_aplicacion(datos_conexion_principal);

            }

            @Override
            public void onError(List<String> errors) {
                // errors
                datos_conexion_principal = false;
                listener.Session_principal_aplicacion(datos_conexion_principal);
            }
        });
    }


}
