package com.example.jiezhou.lib.comunicacion_quickblox.TaskAdmin;



import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by jie.zhou on 13/11/2015.
 */
public class TasksAdmin extends TimerTask {
    private TasksAdminListener listener;
    private static long SPLASH_SCREEN_DELAY ;
    private int iNumOfCycles;
    private Timer timer = null;

    /**
     * el metodo de constructor que hay dos parametro
     * la primero parametro es la tiempo que dura cada ciclo
     * la segundo parametro es la numero de ciclo que necesita hacer
     * @param delay
     * @param numOfCycles
     */
    public TasksAdmin(long delay, int numOfCycles){
        SPLASH_SCREEN_DELAY=delay;
        iNumOfCycles=numOfCycles;
        timer = new Timer();
    }

    /**
     * inicia el tarea
     */
    public void startTasks(){
        timer.schedule(this, SPLASH_SCREEN_DELAY,SPLASH_SCREEN_DELAY);
    }

    /**
     * añadir el listener
     * @param listener
     */
    public void setTasksAdminListener(TasksAdminListener listener){
        this.listener=listener;
    }

    /**
     * la ejecucion de la tarea
     *cuando listener no esta nulo llama a la metodo taskCycleFinished()
     * con un parametro del valor de ciclo de la listener
     * cada vez se resta un ciclo
     * si el valor de ciclo es 0 se cierra el tiempo y llama a la metodo taskCycleFinishedAll() de la listener
     */

    @Override
    public void run() {
        if(listener!=null)listener.taskCycleFinished(iNumOfCycles);
        iNumOfCycles--;
        if(iNumOfCycles==0){
            timer.cancel();
            if(listener!=null)listener.taskCycleFinishedAll();
        }

    }
}
