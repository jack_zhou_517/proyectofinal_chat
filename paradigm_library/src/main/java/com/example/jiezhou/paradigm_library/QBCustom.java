package com.example.jiezhou.lib.comunicacion_quickblox;

import android.os.Bundle;
import android.util.Log;

import com.quickblox.core.Consts;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.request.QBRequestGetBuilder;
import com.quickblox.customobjects.QBCustomObjects;
import com.quickblox.customobjects.model.QBCustomObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jie.zhou on 06/11/2015.
 */
public class QBCustom {
    QBAdminListeners listener;
    String nombreTable;

    /**
     * escuchar los eventos
     *
     * @param Listener
     */
    public void addListeners(QBAdminListeners Listener) {
        this.listener = Listener;
    }

    public void addTable(String nombreTable) {
        this.nombreTable= nombreTable;
    }

    /**
     * obtener los datos que son de tipo de la parametro que pasa
     *
     * @param tipo_lenguaje
     */
    public void obtener_tipo_lenguaje(String tipo_lenguaje) {
        QBRequestGetBuilder requestBuilder = new QBRequestGetBuilder();

        requestBuilder.eq("lang", tipo_lenguaje);


        QBCustomObjects.getObjects(nombreTable, requestBuilder, new QBEntityCallbackImpl<ArrayList<QBCustomObject>>() {
            @Override
            public void onSuccess(ArrayList<QBCustomObject> customObjects, Bundle params) {
                Log.v("test QBCustom", "entrado");
                listener.datos_lenguaje(customObjects);
            }

            @Override
            public void onError(List<String> errors) {
                Log.v("QBCustom", "denegado");
                listener.datos_lenguaje(null);

            }
        });
    }
}
