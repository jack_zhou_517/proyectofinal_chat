package com.example.jiezhou.lib.comunicacion_quickblox;

import com.quickblox.customobjects.model.QBCustomObject;

import java.util.ArrayList;

/**
 * Created by jie.zhou on 06/11/2015.
 *
 */
public interface QBAdminListeners {
    public void Session_principal_aplicacion(boolean conectado);
    public void datos_lenguaje(ArrayList<QBCustomObject> customObjects);


}
