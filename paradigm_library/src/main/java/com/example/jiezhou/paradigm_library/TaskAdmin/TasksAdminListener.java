package com.example.jiezhou.lib.comunicacion_quickblox.TaskAdmin;

/**
 * Created by jie.zhou on 13/11/2015.
 */
public interface TasksAdminListener {
    public void taskCycleFinished(int iNumOfCycles);
    public void taskCycleFinishedAll();
}
