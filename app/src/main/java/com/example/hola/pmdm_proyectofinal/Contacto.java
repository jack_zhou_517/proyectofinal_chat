package com.example.hola.pmdm_proyectofinal;

import java.util.ArrayList;

/**
 * Created by hola on 05/12/2015.
 */
public class Contacto {

public static ArrayList<Contacto> contactos=new ArrayList<>();
    private int imagen;
    private String nombre;
    private String sexo;
    private String descripcion;
    private Contenido ultimoPuclicado;
    public Contacto(int imag,String nombre,String sexo,String descripcion,Contenido ultimoPuclicado){
        this.imagen=imag;
        this.nombre=nombre;
        this.sexo=sexo;
        this.descripcion=descripcion;
        this.ultimoPuclicado=ultimoPuclicado;
    }
    public int getImagen() {
        return imagen;
    }

    public void setImagen(int imagen) {
        this.imagen = imagen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Contenido getUltimoPuclicado() {
        return ultimoPuclicado;
    }

    public void setUltimoPuclicado(Contenido ultimoPuclicado) {
        this.ultimoPuclicado = ultimoPuclicado;
    }
}
