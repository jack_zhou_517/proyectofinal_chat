package com.example.hola.pmdm_proyectofinal;

import android.content.Intent;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import org.json.JSONObject;

import java.util.ArrayList;

public class ChatActivity extends AppCompatActivity {
    private GestureDetectorCompat Detector;
    ListaChat listaChat_adapter;
    ListaContacto listaContacto_adapter;
    ChatActivity chatActivity;

    LinearLayout layout_contacto;
    LinearLayout layout_chatReciente;
    ListView listView_chats;
    ListView listView_contactos;
    EditText editText_nombreUser;

    public static Thread_ConecxionServidor thread_ConecxionServidor;
    public static String nombreUser;
    public static ArrayList<DatosComunicacion_cadaContacto> DatosComunicacion_Contactos=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        DatosComunicacion_Contactos=new ArrayList<>();
        componente_necesario();
        componente_layout();
        init_conexionServidor();
        init_listaChat();
        init_listaContado();
        mensaje.mensajes.add(new mensaje("mark", "yo", "hola Jack"));/*
        mensaje.mensajes.add(new mensaje("yo", "Mark Zuckerberg", "hola Mark"));
        mensaje.mensajes.add(new mensaje("yo", "Mark Zuckerberg", "que tal Mark"));
        mensaje.mensajes.add(new mensaje("Mark Zuckerberg", "yo", "Bien Jack"));
        mensaje.mensajes.add(new mensaje("Bill Gates", "yo", "Me gusta la aplicacion"));
        mensaje.mensajes.add(new mensaje("yo", "Bill Gates", "me alegro que te guste"));
        mensaje.mensajes.add(new mensaje("yo", "Bill Gates", "tienes algun opinion"));
        mensaje.mensajes.add(new mensaje("Bill Gates", "yo", "todavia no me surgido"));*/

    }

    //*** jack:onCreate ***
    private void componente_necesario() {
        this.chatActivity = this;
        listaChat_adapter = new ListaChat(this);
        listaContacto_adapter = new ListaContacto(this);
        Detector = new GestureDetectorCompat(this, new toqueListener_chatActivity(this));
    }

    private void componente_layout() {
        layout_chatReciente = (LinearLayout) findViewById(R.id.layout_chatReciente);

        layout_contacto = (LinearLayout) findViewById(R.id.layout_contacto);

        listView_chats = (ListView) findViewById(R.id.listView_chats);
        listView_contactos = (ListView) findViewById(R.id.listView_contactos);
        editText_nombreUser = (EditText) findViewById(R.id.NombreUser);
    }

    private void init_conexionServidor() {
        thread_ConecxionServidor = new Thread_ConecxionServidor(this);
       thread_ConecxionServidor.start();


    }

    public void init_listaChat() {
        listView_chats.setAdapter(listaChat_adapter);
        listView_chats.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                contenido_ChatReciente chat = (contenido_ChatReciente) listaChat_adapter.getItem(arg2);

                Intent intent = new Intent(chatActivity, PanelMensajeActivity.class);
                intent.putExtra("nombreContacto", chat.getNombre());
                intent.putExtra("imagen", chat.getImagen() + "");
                startActivity(intent);

            }
        });
    }

    private void init_listaContado() {
        listView_contactos.setAdapter(listaContacto_adapter);
        listView_contactos.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                Contacto contacto = (Contacto) listaContacto_adapter.getItem(arg2);
                //  Toast.makeText(getApplicationContext(), contacto.getImagen(), Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(chatActivity, PanelMensajeActivity.class);
                intent.putExtra("nombreContacto", contacto.getNombre());
                intent.putExtra("imagen", contacto.getImagen() + "");
                startActivityForResult(intent, 1);

            }
        });
    }


    //*** /jack:onCreate ***
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {

            if (resultCode == 1) {
                init_listaChat();
            }
        }
    }


    public void nombreUser_servidor(View view) {
        try {
            nombreUser = editText_nombreUser.getText().toString();
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("cmd", "identificacion");
            jsonObj.put("userName", editText_nombreUser.getText());
            thread_ConecxionServidor.enviar_datos_servidor(jsonObj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void layout_visible() {
        getLayout_contacto().setVisibility(View.VISIBLE);
    }

    public void layout_invisible(View v) {
        getLayout_contacto().setVisibility(View.GONE);

    }


    public LinearLayout getLayout_contacto() {
        return layout_contacto;
    }

    public void setLayout_contacto(LinearLayout layout_contacto) {
        this.layout_contacto = layout_contacto;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.Detector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    class toqueListener_chatActivity extends GestureDetector.SimpleOnGestureListener {
        ChatActivity chatActivity;

        public toqueListener_chatActivity(ChatActivity chatActivity) {
            this.chatActivity = chatActivity;
        }

        @Override
        public boolean onDown(MotionEvent event) {

            return true;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            if ((int) (e1.getX() - e2.getX()) > 1) {
                Log.v("testtoque", e1.getX() + "---" + e2.getX());
                chatActivity.layout_visible();
            }
            return true;
        }
    }


}
