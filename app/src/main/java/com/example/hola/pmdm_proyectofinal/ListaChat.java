package com.example.hola.pmdm_proyectofinal;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by hola on 06/12/2015.
 */
public class ListaChat extends BaseAdapter {
    private ChatActivity chatActivity;

    public ListaChat(ChatActivity chatActivity){
        this.chatActivity = chatActivity;

        /*chats.add(new contenido_ChatReciente(R.mipmap.markz, "mark","hello world hhhhhhhhhh hhhhhhhhhhhhh jjjjjjjjj","2:50","programmmer"));
        chats.add(new contenido_ChatReciente(R.mipmap.billgate, "Bill Gates", "me gusta este aplicacion", "2:50", "apps"));
*/

    }


    @Override
    public int getCount() {
        return ChatActivity.DatosComunicacion_Contactos.size();
    }

    @Override
    public Object getItem(int position) {
        return ChatActivity.DatosComunicacion_Contactos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater lf = chatActivity.getLayoutInflater();
        View v = lf.inflate(R.layout.layout_listachat, null, true);



        ImageView imagen = (ImageView)v.findViewById(R.id.imageView);
        TextView nombre = (TextView)v.findViewById(R.id.textView_nombre);
        TextView tiempo = (TextView)v.findViewById(R.id.textView_tiempo);
        TextView texto = (TextView)v.findViewById(R.id.textView_texto);
        TextView tema = (TextView)v.findViewById(R.id.textView_tema);




        nombre.setText(ChatActivity.DatosComunicacion_Contactos.get(position).getC_CR().getNombre());
        tiempo.setText(ChatActivity.DatosComunicacion_Contactos.get(position).getC_CR().getTiempo());
        texto.setText(ChatActivity.DatosComunicacion_Contactos.get(position).getC_CR().getTexto());
        tema.setText(ChatActivity.DatosComunicacion_Contactos.get(position).getC_CR().getTema());


        imagen.setImageResource(ChatActivity.DatosComunicacion_Contactos.get(position).getC_CR().getImagen());

        return v;
    }
}
