package com.example.hola.pmdm_proyectofinal;

import java.util.ArrayList;

/**
 * Created by hola on 09/12/2015.
 */
public class mensaje {
    public static ArrayList<mensaje> mensajes = new ArrayList<>();
    private String nombre_emisor;
    private String nombre_receptor;
    private String contenido;

    public mensaje(String nombre_emisor, String nombre_receptor, String contenido) {
        this.nombre_emisor = nombre_emisor;
        this.nombre_receptor = nombre_receptor;
        this.contenido = contenido;
    }

    public String getNombre_receptor() {
        return nombre_receptor;
    }

    public void setNombre_receptor(String nombre_receptor) {
        this.nombre_receptor = nombre_receptor;
    }

    public String getNombre_emisor() {
        return nombre_emisor;
    }

    public void setNombre_emisor(String nombre_emisor) {
        this.nombre_emisor = nombre_emisor;
    }

    public static ArrayList<com.example.hola.pmdm_proyectofinal.mensaje> getMensajes() {
        return mensajes;
    }

    public static void setMensajes(ArrayList<com.example.hola.pmdm_proyectofinal.mensaje> mensajes) {
        mensaje.mensajes = mensajes;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }
}
