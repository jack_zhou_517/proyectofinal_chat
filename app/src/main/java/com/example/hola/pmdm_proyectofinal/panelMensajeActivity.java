package com.example.hola.pmdm_proyectofinal;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

public class PanelMensajeActivity extends AppCompatActivity {
    ScrollView scrollView;
    LinearLayout linearLayout;
    String nombre_contacto;
    EditText editText_mensaje;
    JSONObject jsonObj = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_panel_mensaje);


        nombre_contacto = getIntent().getStringExtra("nombreContacto");
        setTitle(nombre_contacto);

        componente_layout();
        // Toast.makeText(getApplicationContext(), getIntent().getStringExtra("imagen"), Toast.LENGTH_SHORT).show();

        obtener_mensaje();

        nombre_contacto = getIntent().getStringExtra("nombreContacto");
        setTitle(nombre_contacto);
        componente_layout();
        // Toast.makeText(getApplicationContext(), getIntent().getStringExtra("imagen"), Toast.LENGTH_SHORT).show();

        obtener_mensaje();


    }

    private void componente_layout() {
        scrollView = (ScrollView) findViewById(R.id.scrollView_mensaje);
        linearLayout = (LinearLayout) findViewById(R.id.linearLayout_mensaje);

        editText_mensaje = (EditText) findViewById(R.id.editText_mensaje);


    }

    private void obtener_mensaje() {
        for (int i = 0; i < mensaje.mensajes.size(); i++) {
            // Toast.makeText(getApplicationContext(), mensaje.mensajes.get(i).getNombre_emisor()+"---"+nombre_contacto, Toast.LENGTH_SHORT).show();

            if (mensaje.mensajes.get(i).getNombre_emisor().equalsIgnoreCase(nombre_contacto)) {


                mensajeContrario_formaMuestra(mensaje.mensajes.get(i).getContenido());


            } else if (mensaje.mensajes.get(i).getNombre_receptor().equalsIgnoreCase(nombre_contacto)) {

                mensajePropio_formaMuestra(mensaje.mensajes.get(i).getContenido());


                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = inflater.inflate(R.layout.mensaje_contrario, null);
                ImageView imageView = (ImageView) view.findViewById(R.id.imageView_contrario);
                TextView textView_mensaje = (TextView) view.findViewById(R.id.textView_mensaje);
                imageView.setImageResource(Integer.parseInt(getIntent().getStringExtra("imagen")));
                textView_mensaje.setText(mensaje.mensajes.get(i).getContenido());
                linearLayout.addView(view);
            } else if (mensaje.mensajes.get(i).getNombre_receptor().equalsIgnoreCase(nombre_contacto)) {
                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = inflater.inflate(R.layout.mensaje_propio, null);
                ImageView imageView = (ImageView) view.findViewById(R.id.imageView_propio);
                TextView textView_mensaje = (TextView) view.findViewById(R.id.textView_mensajePropio);
                imageView.setImageResource(Integer.parseInt(getIntent().getStringExtra("imagen")));
                textView_mensaje.setText(mensaje.mensajes.get(i).getContenido());
                linearLayout.addView(view);
            }
        }
    }

    public void mensaje_servidor(View view) {
        setResult(1);
        boolean Existe=false;
        int posicion=0;
        for(int i=0;i<ChatActivity.DatosComunicacion_Contactos.size();i++){
            if(ChatActivity.DatosComunicacion_Contactos.get(i).getNombreContacto().equalsIgnoreCase(nombre_contacto)){
                Existe=true;
                posicion=i;
            }
        }
        if(Existe){
            if( ChatActivity.DatosComunicacion_Contactos.get(posicion).getpMA()==null){
                ChatActivity.DatosComunicacion_Contactos.get(posicion).setpMA(this);

            }
            ChatActivity.DatosComunicacion_Contactos.get(posicion).getC_CR().setTexto(editText_mensaje.getText().toString());
        }else{
            SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

            ChatActivity.DatosComunicacion_Contactos.add(
                    new DatosComunicacion_cadaContacto(nombre_contacto,
                    this,
                    new contenido_ChatReciente(R.mipmap.markz,
                            nombre_contacto,editText_mensaje.getText().toString(),"",s.format(new Date()))));
        }
        if (jsonObj == null) {
            jsonObj = new JSONObject();

        }
        try {
            jsonObj.put("cmd", "mensaje");
            jsonObj.put("emisor", ChatActivity.nombreUser);
            System.out.println("nombre de receptor:");
            jsonObj.put("receptor", nombre_contacto);
            System.out.println("escribir la mensaje:");
            jsonObj.put("contenido", editText_mensaje.getText());
            ChatActivity.thread_ConecxionServidor.enviar_datos_servidor(jsonObj.toString());
            mensaje.mensajes.add(new mensaje(ChatActivity.nombreUser, nombre_contacto, editText_mensaje.getText().toString()));

            mensajePropio_formaMuestra(editText_mensaje.getText().toString());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void mensajePropio_formaMuestra(String mensaje) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view1 = inflater.inflate(R.layout.mensaje_propio, null);
        ImageView imageView = (ImageView) view1.findViewById(R.id.imageView_propio);
        TextView textView_mensaje = (TextView) view1.findViewById(R.id.textView_mensajePropio);
        imageView.setImageResource(Integer.parseInt(getIntent().getStringExtra("imagen")));
        Log.v("testmensaje", mensaje);

        textView_mensaje.setText(mensaje);
        linearLayout.addView(view1);
    }

    public void mensajeContrario_formaMuestra(String mensaje) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.mensaje_contrario, null);
        ImageView imageView = (ImageView) view.findViewById(R.id.imageView_contrario);
        TextView textView_mensaje = (TextView) view.findViewById(R.id.textView_mensaje);
        imageView.setImageResource(Integer.parseInt(getIntent().getStringExtra("imagen")));
        textView_mensaje.setText(mensaje);
        linearLayout.addView(view);
    }

}
