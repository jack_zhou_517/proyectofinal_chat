package com.example.hola.pmdm_proyectofinal;

/**
 * Created by hola on 05/12/2015.
 */
public class contenido_ChatReciente {
    private int imagen;
    private String nombre;
    private String texto;
    private String tiempo;
    private String tema;
    public contenido_ChatReciente(int imag, String nombre, String texto, String tema, String tiempo){
        this.imagen=imag;
        this.nombre=nombre;
        this.texto=texto;
        this.tiempo=tiempo;
        this.tema=tema;
    }
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getImagen() {
        return imagen;
    }

    public void setImagen(int imagen) {
        this.imagen = imagen;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getTiempo() {
        return tiempo;
    }

    public void setTiempo(String tiempo) {
        this.tiempo = tiempo;
    }

    public String getTema() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema = tema;
    }
}
