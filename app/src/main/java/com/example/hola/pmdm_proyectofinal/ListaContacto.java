package com.example.hola.pmdm_proyectofinal;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by hola on 06/12/2015.
 */
public class ListaContacto extends BaseAdapter {
    private ChatActivity chatActivity;

    public ListaContacto(ChatActivity chatActivity){
        this.chatActivity = chatActivity;

        obtener_datos_contactor();

    }
    private void obtener_datos_contactor(){
        Contacto.contactos.add(new Contacto(R.mipmap.markz, "mark", "H", "Creador de Facebook", null));
        Contacto.contactos.add(new Contacto(R.mipmap.marissamayer, "Marissa Mayer","M","CEO yahoo",null));
        Contacto.contactos.add(new Contacto(R.mipmap.billgate, "Bill Gates","H","No soy rico,tampoco soy poble",null));

    }

    @Override
    public int getCount() {
        return Contacto.contactos.size();
    }

    @Override
    public Object getItem(int position) {
        return Contacto.contactos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater lf = chatActivity.getLayoutInflater();
        View v = lf.inflate(R.layout.layout_listacontacto, null, true);



        ImageView imagen = (ImageView)v.findViewById(R.id.imageView);
        TextView nombre = (TextView)v.findViewById(R.id.textView_nombre);
        TextView descripcion = (TextView)v.findViewById(R.id.textView_descripcion);
        LinearLayout publicado = (LinearLayout)v.findViewById(R.id.layout_publicado);
        LinearLayout itemContacto = (LinearLayout)v.findViewById(R.id.layout_itemContacto);



        nombre.setText(Contacto.contactos.get(position).getNombre());
        descripcion.setText(Contacto.contactos.get(position).getDescripcion());

       if (Contacto.contactos.get(position).getSexo().equalsIgnoreCase("H")){
           itemContacto.setBackgroundColor(0xFFAAC5FA);
       }else{
           itemContacto.setBackgroundColor(0xFFFAA4F2);
       }


        imagen.setImageResource(Contacto.contactos.get(position).getImagen());

        return v;
    }

}
