package com.example.hola.pmdm_proyectofinal;

/**
 * Created by hola on 09/01/2016.
 */
public class DatosComunicacion_cadaContacto {


    private String nombreContacto;
    private PanelMensajeActivity pMA;
    private contenido_ChatReciente c_CR;
   public DatosComunicacion_cadaContacto(String nombreContacto,PanelMensajeActivity pMA,contenido_ChatReciente c_CR){
    this.nombreContacto=nombreContacto;
       this.pMA=pMA;
       this.c_CR=c_CR;
    }

    public PanelMensajeActivity getpMA() {
        return pMA;
    }

    public void setpMA(PanelMensajeActivity pMA) {
        this.pMA = pMA;
    }

    public String getNombreContacto() {
        return nombreContacto;
    }

    public void setNombreContacto(String nombreContacto) {
        this.nombreContacto = nombreContacto;
    }

    public contenido_ChatReciente getC_CR() {
        return c_CR;
    }

    public void setC_CR(contenido_ChatReciente c_CR) {
        this.c_CR = c_CR;
    }

}
