package com.example.hola.pmdm_proyectofinal;

import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by hola on 06/01/2016.
 */
public class Thread_ConecxionServidor extends Thread{
    private Socket socket;
    private ChatActivity chatActivity;

    private static final int SERVERPORT = 5000;

    private static final String SERVER_IP = "192.168.1.39";

    private BufferedReader in = null;
    private PrintWriter out = null;
    public Thread_ConecxionServidor(ChatActivity chatActivity){
        this.chatActivity=chatActivity;
    }
    @Override
    public void run() {
        try {
            InetAddress serverAddr = InetAddress.getByName(SERVER_IP);

            socket = new Socket(serverAddr, SERVERPORT);

                in = new BufferedReader(new InputStreamReader(socket
                        .getInputStream()));
                out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(
                        socket.getOutputStream())), true);

            while(true){
                String respuesta=in.readLine();
                Log.v("respuesta", respuesta.substring(0, respuesta.indexOf(",")));
                String emisor =respuesta.substring(0, respuesta.indexOf(","));
                String mensaje_emisor=respuesta.substring(respuesta.indexOf(",")+1,respuesta.length());
                mensaje.mensajes.add(new mensaje(emisor, "", mensaje_emisor));
                boolean Existe=false;
                int posicion=0;
                for(int i=0;i<ChatActivity.DatosComunicacion_Contactos.size();i++){
                    if(ChatActivity.DatosComunicacion_Contactos.get(i).getNombreContacto().equalsIgnoreCase(emisor)){
                        Existe=true;
                        posicion=i;
                    }
                }
                if(Existe){
                    if(!( ChatActivity.DatosComunicacion_Contactos.get(posicion).getpMA()==null)){
                        ChatActivity.DatosComunicacion_Contactos.get(posicion).getpMA().mensajeContrario_formaMuestra(mensaje_emisor);
                    }
                    ChatActivity.DatosComunicacion_Contactos.get(posicion).getC_CR().setTexto(mensaje_emisor);

                }else{

                    ChatActivity.DatosComunicacion_Contactos.add(
                            new DatosComunicacion_cadaContacto(emisor,
                                    null,
                                    new contenido_ChatReciente(R.mipmap.markz,
                                            emisor, mensaje_emisor, "", "")));


                    Log.v("respuesta", emisor);

                }
                chatActivity.init_listaChat();
            }

        } catch (UnknownHostException e1) {
            e1.printStackTrace();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }
    public void enviar_datos_servidor(String datos){
        out.println(datos);
        //vacia el buffer
        out.flush();
    }

}
